use colorsys::{Hsl, Rgb};
use crossterm::style::{
    style, Color,
    Color::{AnsiValue, Black, White},
};
use itertools::Itertools;
use terminal_size::terminal_size;

fn show8bit(color: u8) {
    let fg = match color {
        0x0..=0x7 | 0xe8..=0xf3 => White,
        c @ 0x10..=0xe7 if (c - 0x10) / 18 % 2 == 0 => White,
        _ => Black,
    };
    let text = match color {
        0x0..=0xf => format!("        {:02x}        ", color),
        0x10..=0xe7 => format!(" {:02x} ", color),
        0xe8..=0xff => format!("  {:02x}  ", color),
    };
    print!("{}", style(text).with(fg).on(AnsiValue(color)))
}

fn show_two(first: Color, second: Color) {
    print!(
        "{}",
        style("\u{2580}")
            .with(first)
            .on(second)
    )
}

fn main() {
    let canvas_height = std::env::args()
        .skip(1)
        .next()
        .map(|i| i.parse().ok())
        .flatten().or(terminal_size().map(|s| (s.1.0 - 9) * 2))
        .unwrap_or(144);
    for i in 0x0..=0x7 {
        show8bit(i)
    }
    println!();
    for i in 0x8..=0xf {
        show8bit(i)
    }
    println!();
    for i in (0x10..=0xe7).step_by(6 * 6) {
        for j in (i..i + 6 * 6).step_by(6) {
            for k in j..j + 6 {
                show8bit(k)
            }
        }
        println!();
    }
    for i in 0xe8..=0xff {
        show8bit(i)
    }
    println!();
    for (l1, l2) in (0..canvas_height)
        .map(|i| i as f64 * 100. / canvas_height as f64)
        .chain(std::iter::once(std::f64::NAN))
        .tuples()
    {
        for h in (0..144).map(|i| i as f64 * 2.5) {
            let rgb1: Rgb = Hsl::new(h, 50., l1, None).into();
            if l2.is_nan() {
                print!(
                    "{}",
                    style(String::from_utf16(&[0x2580]).unwrap()).with(Color::Rgb {
                        r: rgb1.get_red() as u8,
                        g: rgb1.get_green() as u8,
                        b: rgb1.get_blue() as u8,
                    })
                )
            } else {
                let rgb2: Rgb = Hsl::new(h, 50., l2, None).into();
                show_two(
                    Color::Rgb {
                        r: rgb1.get_red() as u8,
                        g: rgb1.get_green() as u8,
                        b: rgb1.get_blue() as u8,
                    },
                    Color::Rgb {
                        r: rgb2.get_red() as u8,
                        g: rgb2.get_green() as u8,
                        b: rgb2.get_blue() as u8,
                    },
                )
            }
        }
        println!()
    }
}
